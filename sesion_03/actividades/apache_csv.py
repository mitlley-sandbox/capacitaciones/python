"""
    Script que lee un archivo de logs de apache y entrega la suma de las
    solicitudes por ip.
"""
import re
from collections import Counter
import csv

def leer_log(archivo_log):
    """
        Funcion que lee el log de acceso linea por linea y agrega filas al archivo csv
    """

    # abrimos un archivo csv
    with open('access.csv', 'w') as archivo_csv:
        # objeto que ira escribiendo sobre el archivo csv
        csv_escritor = csv.writer(archivo_csv, delimiter=',', quotechar='"')

        # agregamos la primera fila, generalmente considerada la cabecera
        csv_escritor.writerow(['ip', 'fecha', 'metodo'])

        with open(archivo_log) as file:
            # leemos la primera linea
            linea = file.readline()
            i = 1
            while linea:
                # expresion regular para cada elemento
                ipregex = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
                dateregex = r'\d{2}\/\w{3}\/\d{4}\:\d{2}\:\d{2}\:\d{2}\s?\+\d{4}'
                metodoregex = r'(\")(\w{3,4})(\s)'

                # obtener cada elemento
                ip = re.search(ipregex, linea).group()
                fecha = re.search(dateregex, linea).group()
                metodo = re.search(metodoregex, linea).group(2)

                # creamos la fila
                lista = [ip, fecha, metodo]

                #agregamos la fila al archivo
                csv_escritor.writerow(lista)
                #print(lista)
                linea = file.readline()
                i += 1

# Create entry point of our code
if __name__ == '__main__':
    leer_log("access.log")
