"""
    Script que lee un archivo de logs de apache y entrega la suma de las
    solicitudes por ip.
"""
import re
from collections import Counter

def leer_log(archivo_log):
    ipregex = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'

    with open(archivo_log) as file:
        log = file.read()
        lista_ips = re.findall(ipregex, log)
        cuenta_ips = Counter(lista_ips)
        cuenta_ips_lista = cuenta_ips.items()

        cuenta_ips_lista.sort(key = lambda x : x[1], reverse = True)

        for k, v in cuenta_ips_lista:
            print("IP\t%s\tCuenta\t%s" % (str(k), str(v)))

# Create entry point of our code
if __name__ == '__main__':
    leer_log("access.log")
