"""
    Script que lee un archivo de logs de apache y entrega la suma de las
    solicitudes por ip.
"""
import re
from collections import Counter
from openpyxl import Workbook

def leer_log(archivo_log):
    """
        Funcion que lee el log de acceso linea por linea y agrega filas al archivo excel
    """

    # creamos el libro
    libro = Workbook()
    hoja = libro.active

    with open(archivo_log) as file:
        # leemos la primera linea
        linea = file.readline()
        i = 1
        while linea:
            # expresion regular para cada elemento
            ipregex = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'
            dateregex = r'\d{2}\/\w{3}\/\d{4}\:\d{2}\:\d{2}\:\d{2}\s?\+\d{4}'
            metodoregex = r'(\")(\w{3,4})(\s)'

            # obtener cada elemento
            ip = re.search(ipregex, linea).group()
            fecha = re.search(dateregex, linea).group()
            metodo = re.search(metodoregex, linea).group(2)

            # creamos la fila
            lista = [ip, fecha, metodo]

            #agregamos la fila a la hoja
            hoja.append(lista)
            #print(lista)
            linea = file.readline()
            i += 1

    libro.save('access.xlsx')

# Create entry point of our code
if __name__ == '__main__':
    leer_log("access.log")
