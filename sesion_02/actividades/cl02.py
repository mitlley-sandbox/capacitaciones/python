# -*- coding: utf-8 -*-
class ProcesadorArchivos:
    def leer(self, archivo):
        with open(archivo, 'r') as file:
            datos = file.read()
            return datos

    def escribir(self, archivo, datos):
        with open(archivo, 'w') as file:
            file.write(str(datos))

# pedir nombre de 3 alumnos

lista_alumnos = []
lista_carreras = []

def preguntar_cantidad_alumnos():
    try:
        maximo_alumnos = int(raw_input("Cuantos alumnos debemos ingresar? "))
        return maximo_alumnos
    except:
        print("Ingrese un numeor valido por favor.")
        return preguntar_cantidad_alumnos()

def llenar_listas(maximo_alumnos):
    print("Ingrese el nombre de %d alumnos" % (maximo_alumnos))
    contador = 1

    while contador <= maximo_alumnos:
        # pedir el nombre de cada alumno
        nombre = raw_input("Nombre del %dº alumno: " % (contador))

        # lista temporal con las carreras preferidas por el alumno
        carreras_alumno = []
        contador_carreras = 1
        while contador_carreras <= 3:
            carrera = raw_input("\t%dº carrera de %s: " % (contador_carreras,
                                                            nombre))
            carreras_alumno.append(carrera)
            contador_carreras = contador_carreras + 1

        lista_carreras.append(carreras_alumno)
        lista_alumnos.append(nombre)

        # esta es la forma de incrementar un numero en python
        contador = contador +1

    # hacer la persistencia de las listas
    procesador = ProcesadorArchivos()
    procesador.escribir('lista_alumnos', lista_alumnos)
    procesador.escribir('lista_carreras', lista_carreras)

def listar_alumnos():
    """ Funcion que imprime ordenadamente la lista de alumnos """
    # Mostra los nombres de los alumnos ingresados
    print("Estos son los alumnos ingresados:")
    for alumno in lista_alumnos:
        print(" > %s" % alumno)

def buscar_alumno():
    """ Solicitamos el nombre del alumno y lo buscamos en la lista """
    # Buscar un alumno en particular
    nombre_busqueda = raw_input('Que alumno desea consultar? ')

    # que Python haga la busqueda por nosotros
    try:
        indice_alumno = lista_alumnos.index(nombre_busqueda)

        print("Las carreras del alumno:")
        for carrera in lista_carreras[indice_alumno]:
            print(" - %s" % carrera)
    except ValueError:
        print("Error, debe ingresar un nombre valido")
        buscar_alumno()


maximo_alumnos = preguntar_cantidad_alumnos()
llenar_listas(maximo_alumnos)
listar_alumnos()
buscar_alumno()
