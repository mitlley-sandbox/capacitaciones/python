class ProcesadorArchivos:
    def leer(self, archivo):
        with open(archivo, 'r') as file:
            datos = file.read()
            return datos

    def escribir(self, archivo, datos):
        with open(archivo, 'w') as file:
            file.write(str(datos))

procesador = ProcesadorArchivos()
procesador.escribir('test', [0, 1])
print("Datos guardados")
print(procesador.leer('test'))
