import json

class ProcesadorArchivos:
    def leer(self, archivo):
        with open(archivo, 'r') as file:
            datos = json.loads(file.read())
            return datos

    def escribir(self, archivo, datos):
        with open(archivo, 'w') as file:
            datos = json.loads(datos)
            print(datos['main']['temp'])
            file.write(json.dumps(datos))
