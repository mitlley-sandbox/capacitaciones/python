# declaramos un diccionario
puntajes = {'luz': 4.3}

# abrimos el archivo en modo escritura
file = open('data', 'w')

# escribimos en el archivo
file.write(str(puntajes))

# cerramos el archivo por seguridad
file.close()

# escritura de los datos
with open('data', 'w') as file:
    file.write(str(puntajes))

# lectura de los datos
with open('data', 'r') as file:
    datos = file.read()
    print(datos)
