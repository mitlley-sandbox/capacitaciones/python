class Persona:
    nombre = "Cristian"

    def __init__(self, nombre):
        self.nombre = nombre

    def saludar(self):
        print("Hola, soy %s!" % self.nombre)


# instanciar un objeto
fulano = Persona('Daniel')
fulano.saludar() # Hola, soy Daniel!

# agregar el apellido
fulano.apellido = 'Soto'

print('Hola, soy %s %s!' % (fulano.nombre, fulano.apellido))
