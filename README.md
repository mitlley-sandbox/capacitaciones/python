# Curso de Python

En este proyecto de git usted encontrará el código que acompaña el curso de Python. Además se proporcionarán los enlaces a las documentaciones y material de estudio utilizado a lo largo de curso.

El presente proyecto será actualizado a medida que se avanza en el curso.

## Enlaces
* [Python - Sitio oficial](https://python.org)
* [Python Package Index (PyPI)](https://pypi.org)
* [Documentación openpyxl](https://openpyxl.readthedocs.io/en/stable/)
* [Documentacion re](https://platzi.com/blog/expresiones-regulares-python/)

* [Presentación](https://docs.google.com/presentation/d/1GLbCWiKErHskg8MNjkhkPaQaeK5aXFuUmuL6FHmg9Ao/edit?usp=sharing)
* [Presentación día 2](https://docs.google.com/presentation/d/1Pa75pRJ6l0ZFa1uQVGLuFJs38JyWBCeR4-FOrp38aHE/edit?usp=sharing)
* [Presentación día 3](https://docs.google.com/presentation/d/1sjb82V1_IRBzqP-SKh5lositNx8Rko5g6NhYEI5zvuY/edit?usp=sharing)
