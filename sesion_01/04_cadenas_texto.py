print("Trabajo con cadenas de texto.")
print("")

# declaracion de una cadena de texto
texto = "Este es un texto en una variable"

#funcion len para contar el largo de la cadena
print("'%s' contiene %d caracteres" % (texto, len(texto)))

#encuentre la x
print("La 'x' esta en la posicion %d" % texto.index('x'))

# cuenta la letra e
print("La 'e' aparece %d veces" % texto.count('e'))
