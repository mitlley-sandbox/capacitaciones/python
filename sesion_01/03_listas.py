print("Trabajo con listas.")
print("")

# Declaramos una lista vacia
lista = []

# agregamos un valor
lista.append('Diego')
lista.append('Pedro')

# accedemos a su valor
print("Hola %s"% lista[1])

# podemos agregar otros tipos de datos
lista.append(7)

print("Imprimiendo todo los items de la lista")
for item in lista:
    # notese la identacion
    print(item)

# cuantos elementos contiene una lista
print("La lista contiene %d elementos" % len(lista))

i = 0
while(i < len(lista)):
    print(lista[i])
    i += 1

# Eliminar elemento de la lista
lista.pop()

print(lista)

# Operaciones sobre las listas
nueva_lista = ['Luz', 'Mario']
lista_completa = lista + (nueva_lista * 2)

print(lista_completa.index('Diego'))
