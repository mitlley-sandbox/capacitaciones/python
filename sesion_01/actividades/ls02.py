# -*- coding: utf-8 -*-
# pedir nombre de 3 alumnos

lista_alumnos = []
lista_carreras = []
contador = 1

maximo_alumnos = int(raw_input("Cuantos alumnos debemos ingresar? "))

print("Ingrese el nombre de %d alumnos" % (maximo_alumnos))

while contador <= maximo_alumnos:
    # pedir el nombre de cada alumno
    nombre = raw_input("Nombre del %dº alumno: " % (contador))

    # lista temporal con las carreras preferidas por el alumno
    carreras_alumno = []
    contador_carreras = 1
    while contador_carreras <= 3:
        carrera = raw_input("\t%dº carrera de %s: " % (contador_carreras, nombre))
        carreras_alumno.append(carrera)
        contador_carreras = contador_carreras + 1

    lista_carreras.append(carreras_alumno)
    lista_alumnos.append(nombre)

    # esta es la forma de incrementar un numero en python
    contador = contador +1

# mostrar la lista completa
print(lista_alumnos)
print(lista_carreras)
