# -*- coding: utf-8 -*-
# solicitar nombre
nombre = raw_input("Cual es nu nombre? ")
apellido = raw_input("Muchas gracias %s, puedes indicarme tu apellido? " % nombre)
nacimiento = raw_input("Que año naciste %s %s? " % (nombre, apellido))
nacimiento = int(nacimiento)

#calcular la edad del usuario
edad = 2019 - int(nacimiento)
generacion = None

if nacimiento <= 2010 and nacimiento >= 1994:
    generacion = "Z"

elif nacimiento <= 1993 and nacimiento >= 1981:
    generacion = "Y"

elif nacimiento <= 1980 and nacimiento >= 1969:
    generacion = "X"

elif nacimiento <= 1968 and nacimiento >= 1949:
    generacion = "Baby Boom"

elif nacimiento <= 1948 and nacimiento >= 1930:
    generacion = "Silent Generation"

else:
    generacion = "No identificada"

print("Muchas gracias %s, hasta pronto! Tu edad es %d, por ende tu generacion es %s" % (nombre, edad, generacion))
