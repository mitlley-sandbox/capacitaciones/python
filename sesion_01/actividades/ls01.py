# -*- coding: utf-8 -*-
# pedir nombre de 3 alumnos

lista_alumnos = []
contador = 1

maximo_alumnos = int(raw_input("Cuantos alumnos debemos ingresar? "))

print("Ingrese el nombre de %d alumnos" % (maximo_alumnos))

while contador <= maximo_alumnos:
    # pedir el nombre de cada alumno
    nombre = raw_input("Nombre del %dº alumno: " % (contador))
    lista_alumnos.append(nombre)

    # esta es la forma de incrementar un numero en python
    contador = contador +1

# mostrar la lista completa
print(lista_alumnos)
