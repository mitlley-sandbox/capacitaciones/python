print("Bienvenido!\nProbemos las condiciones...")
condicion = 2 < 1

if condicion:
    print("La condicion se cumple")

else:
    print("Lo sentimos, la condicion no se cumple")
    print("Este es un segundo mensaje dentro del mismo bloque")

print("Este mensaje de despedida aparece siempre")
