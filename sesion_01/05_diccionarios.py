print("Trabajo con diccionarios.")
print("")

# declaracion de un diccionario vacio
alumnos = {}
alumnos['Pedro'] = 6.1

# manera alternativa
alumnos = {
    'Pedro': 6.1
}

#iterar sobre los valores
for nombre, nota in alumnos.iteritems():
    print "La nota de %s es %d" % (nombre, nota)

# eliminamos un alumno del diccionario
alumnos.pop('Pedro')
print(alumnos)
