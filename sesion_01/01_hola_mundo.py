# Esto imprime "Hola, Juan!"
nombre = "Juan"
print("Hola, %s!" % nombre)

# declaracion de una variable
sujeto = "Mundo"

# formateo de un string
saludo = "Hola %s%s" % (sujeto, "!")
print(saludo)

#Tipos de datos
print("La variable saludo es de tipo %s" % type(saludo))

# solicitar datos
nombre = raw_input("Cual es nu nombre? ")
print("Hola %s" % nombre)
